
## Helpers

$ sudo kill -9 $(sudo lsof -t -i:8888) // to kill a port

$ sudo service redis-commander restart // to restart Redis

$ sudo service redis-commander start // to start Redis

$ redis-commander // to run Redis

$ npm run devStart // to start Node

$ npm start // to start React
