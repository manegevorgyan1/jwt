module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    container: {
      center: true,
    },

    extend: {
      colors: {
        'green' : {
          'main-green': '#688F4E',
          'main-green-hover' : '#688F8E',
          'deep-green' : '#2B463C',
          'light-green' : '#B1D182',
          'nude' : '#F4F1E9',
          'background' : '#E5E5E5',
          'footer-title' : '#FFD2DD',
        },
        'grey' : {
          'secondary' : '#696F79',
          'bg' : '#f7f7f7',
        }
      },
      fonts: {
        'main' : 'Poppins',
      },
      

    },
  },
  plugins: [],
}