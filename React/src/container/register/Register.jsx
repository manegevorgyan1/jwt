import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { Menu } from "../../layouts/header/Menu";
import { SAGA_REGISTER } from "../../store/saga/sagatype";
import './register.css';

export const Register = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();


  const handleSubmit = (event) => {
    event.preventDefault();
    const data = new FormData(event.currentTarget);
    dispatch({
      type: SAGA_REGISTER,
      data: {
        name: data.get("firstName"),
        surname: data.get("lastName"),
        email: data.get("email"),
        password: data.get("password"),
      },
      navigate,
    });
  };

  return (
    <>
    <Menu />
      <form action="/register" method="post" onSubmit={handleSubmit} className="w-25 mx-auto">
        <h1>Sign up</h1>
        <div className="mb-3 mt-3">
          <label className="form-label">Name:</label>
          <input
            type="text"
            className="form-control"
            placeholder="Enter Name"
            name="firstName"
            required
            autoFocus
          />
        </div>
        <div className="mb-3 mt-3">
          <label className="form-label">Surname:</label>
          <input
            type="text"
            className="form-control"
            placeholder="Enter Surname"
            name="lastName"
          />
        </div>
        <div className="mb-3 mt-3">
          <label className="form-label">Email:</label>
          <input
            type="email"
            className="form-control"
            id="email"
            placeholder="Enter email"
            name="email"
          />
        </div>
        <div className="mb-3">
          <label className="form-label">Password:</label>
          <input
            type="password"
            className="form-control"
            id="pwd"
            placeholder="Enter password"
            name="password"
          />
        </div>
        <div className="form-check mb-3">
          <label className="form-check-label">
            <input
              className="form-check-input"
              type="checkbox"
              name="remember"
            />{" "}
            Remember me
          </label>
        </div>
        <button type="submit" className="btn btn-primary bg-primary">
          Submit
        </button>
      </form>
    </>
  );
};
