import { Menu } from '../../layouts/header/Menu';
import './welcome.css';

export const Welcome = () => {

  return (
    <>
      <Menu></Menu>
      <h1>Please login to continue</h1>
    </>
  );
};
