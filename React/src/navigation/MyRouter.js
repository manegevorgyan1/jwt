import { BrowserRouter, Route, Routes } from "react-router-dom";
import { Home } from "../container/homePage/Home.jsx";
import { Login } from "../container/login/Login.jsx";
import { Register } from "../container/register/Register.jsx";
import { Welcome } from "../container/welcomePage/Welcome.jsx";

export const MyRouter = () => {
  return (
    <div>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Welcome />} />
          <Route path="/home" element={<Home />} />
          <Route path="/login" element={<Login />} />
          <Route path="/register" element={<Register />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
};
