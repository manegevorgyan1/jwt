export const SAGA_REGISTER = 'sagaRegister';

export const SAGA_LOGIN = 'sagaLogin';

export const SAGA_PROFILE = 'sagaProfile';

export const SAGA_LOGOUT = 'sagaLogout';
