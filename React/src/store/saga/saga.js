import Axios from 'axios';

import { call, put, select, takeEvery } from 'redux-saga/effects';

import {
  SAGA_LOGIN,
  SAGA_LOGOUT,
  SAGA_PROFILE,
  SAGA_REGISTER,
} from './sagatype';

import { LINK_LOGIN, LINK_LOGOUT, LINK_PROFILE, LINK_REGISTER } from './link';

import { reducerProfile, reducerStatus, reducerUser } from '../data/action';

import Swal from 'sweetalert2';


// import '../axiosConfig';

const axios = Axios.create({
  withCredentials: true,
});

const setToken = (token) => {
  localStorage.setItem('token', token);
  localStorage.setItem('lastLoginTime', new Date(Date.now()).getTime());
};

const deleteToken = () => {
  localStorage.removeItem('token');
  localStorage.removeItem('lastLoginTime');
};

const setHeader = (token) => {
  axios.interceptors.request.use(
    function (request) {
      request.headers['Authorization'] = `Bearer ${token}`;
      return request;
    },
    null,
    { synchronous: true }
  );
};

export function* mySaga() {
  yield takeEvery(SAGA_REGISTER, register);
  yield takeEvery(SAGA_LOGIN, login);
  yield takeEvery(SAGA_PROFILE, profile);
  yield takeEvery(SAGA_LOGOUT, logout);
}

function* logout() {
  yield call(axiosLink, {
    link: LINK_LOGOUT,
    data: {},
    fun: reducerStatus,
  });
  deleteToken();
  window.location.href = '/';
}

function* login(action) {
  yield call(axiosLink, {
    link: LINK_LOGIN,
    data: action.data,
    fun: reducerUser,
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
  });
  const data = yield select((st) => st.reducer.user);
  if ('accessToken' in data) {
    setToken(data.accessToken);
    setHeader(data.accessToken);
    action.navigate('/home');
  } else {
    Swal.fire({
      icon: 'error',
      title: 'Oops...',
      text: data.error,
    });
  }
}

function* register(action) {
  yield call(axiosLink, {
    link: LINK_REGISTER,
    data: action.data,
    fun: reducerStatus,
  });
  const data = yield select((st) => st.reducer.status);
  if ('accessToken' in data) {
    action.navigate('/login');
  } else {
    Swal.fire({
      icon: 'error',
      title: 'Oops...',
      text: data.error,
    });
  }
}

function* profile() {
  yield call(axiosLink, {
    link: LINK_PROFILE,
    data: {},
    fun: reducerProfile,
  });
}

/** axios **/
function* axiosLink({ link, fun, data, meth = 'post' }) {
  let res = yield axios({
    method: meth,
    url: link,
    data,
  });
  yield put(fun(res.data));
}
