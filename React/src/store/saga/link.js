export const LINK_REGISTER = 'http://localhost:8888/register';

export const LINK_LOGIN = 'http://localhost:8888/login';

export const LINK_PROFILE = 'http://localhost:8888/home';

export const LINK_LOGOUT = 'http://localhost:8888/logout';