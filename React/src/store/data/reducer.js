import { data } from './data';
import {  
  TYPE_PROFILE,
  TYPE_STATUS,
  TYPE_USER,
  // TYPE_ALERT_CLEAR,
  // TYPE_ALERT_ERROR,
  // TYPE_ALERT_SUCCESS,
  // TYPE_GETALL_FAILUREdd,
  // TYPE_GETALL_REQUESTdd,
  // TYPE_GETALL_SUCCESSdd,
  // TYPE_LOGIN_FAILUREdd,
  // TYPE_LOGIN_REQUESTdd,
  // TYPE_LOGIN_SUCCESSdd,
  // TYPE_LOGOUTdd,
} from './type';

import { AUTHENTICATED, NOT_AUTHENTICATED } from './type';

const initialState = {
  data,
  authChecked: false,
  loggedIn: false,
  currentUser: {},
};

export const reducer = (state = data, action) => {
  const temp = { ...state };
  switch (action.type) {
    case TYPE_PROFILE:
      temp.profile = action.value;
      break;
    case TYPE_STATUS:
      temp.status = action.value;
      break;
    case TYPE_USER:
      temp.user = action.value;
      break;

      case AUTHENTICATED:
      return {
        authChecked: true,
        loggedIn: true,
        currentUser: action.payload,
      };
    case NOT_AUTHENTICATED:
      return {
        authChecked: true,
        loggedIn: false,
        currentUser: {},
      };

    default:
      break;
  }

  return temp;
};



// /// alertReducer
// case TYPE_ALERT_SUCCESS:
//   temp.status = action.value;
//   break;
// case TYPE_ALERT_ERROR:
//   temp.status = action.value;
//   break;
// case TYPE_ALERT_CLEAR:
//   return {};
// /// authenticationReducer
// case TYPE_LOGIN_REQUESTdd:
//   return {
//     loggingIn: true,
//     user: action.user,
//   };
// case TYPE_LOGIN_SUCCESSdd:
//   return {
//     loggedIn: true,
//     user: action.user,
//   };
// case TYPE_LOGIN_FAILUREdd:
//   return {};
// case TYPE_LOGOUTdd:
//   return {};
// /// userReducer
// case TYPE_GETALL_REQUESTdd:
//   return {
//     loading: true
//   }
// case TYPE_GETALL_SUCCESSdd:
//   return {
//     items: action.users
//   }
// case TYPE_GETALL_FAILUREdd:
//   return {
//     error: action.error
//   }
