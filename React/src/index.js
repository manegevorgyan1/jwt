import "./index.css";
import React from "react";
import ReactDOM from "react-dom/client";
import { MyRouter } from "./navigation/MyRouter";
import reportWebVitals from "./reportWebVitals";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import storeRoot from './store/root'
import createSaga from "redux-saga";
import { mySaga } from "./store/saga/saga";
import { createLogger } from 'redux-logger';

import axios from "axios";

import jwt_decode from 'jwt-decode';

const saga = createSaga();

// const loggerMiddleware = createLogger();

// const myStore = createStore(storeRoot, applyMiddleware(saga, loggerMiddleware));
const myStore = createStore(storeRoot, applyMiddleware(saga));

// const axiosPrivate = axios.create({ baseURL: 'http://localhost:8888/' });

// axiosPrivate.interceptors.request.use(
//   async (config) => {
//     const user = myStore?.getState()?.userData?.user;

//     let currentDate = new Date();
//     if (user?.accessToken) {
//       const decodedToken = jwt_decode(user?.accessToken);
//       if (decodedToken.exp * 1000 < currentDate.getTime()) {
//         await myStore.dispatch(refreshToken());
//         if (config?.headers) {
//           config.headers['authorization'] = `Bearer ${
//             myStore?.getState()?.userData?.user?.accessToken
//           }`;
//         }
//       }
//     }
//     return config;
//   },
//   (error) => {
//     return Promise.reject(error);
//   }
// );

saga.run(mySaga);

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <Provider store={myStore}>
      <MyRouter />
    </Provider>
  </React.StrictMode>
);

reportWebVitals();
