const express = require('express');
const router = express.Router();
const PostController = require('../controller/PostController');
const { verifyAccessToken } = require('../helpers/jwt_helper');

router.use(express.json());

router.post('/register', PostController.register);
router.post('/login', PostController.login);

// router.use(verifyAccessToken());

router.post('/home', verifyAccessToken, PostController.home);
router.post('/logout', PostController.logout);
router.post('/getAllUsers', PostController.getAllUsers);
router.post('/getSingleUser/:id', PostController.getSingleUser);
// router.patch('/updateUser/:id', PostController.updateUser);
// router.delete('/deleteUser/:id', PostController.deleteUser);

router.post('/refreshToken', PostController.refreshToken);

router.post('/logout', PostController.logout);

module.exports = router;

