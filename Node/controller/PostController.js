require('dotenv').config();
const User = require('../model/user');
const createError = require('http-errors');
const client = require('../helpers/init_redis');
const {
  registerSchema,
  loginSchema,
} = require('../helpers/validation_schema.js');
const {
  signAccessToken,
  signRefreshToken,
  verifyRefreshToken,
} = require('../helpers/jwt_helper');

class PostController {

  static async home(req, res) {
    const userId = req.payload.aud;
    res.send({ userId });
  }

  static async register(req, res, next) {
    try {
      const result = await registerSchema?.validateAsync(req.body);

      const doesExist = await User.findOne({ email: result?.email });

      if (doesExist) {
        throw createError.Conflict(`${result?.email} is already registered`);
      }

      const user = new User(result);
      const savedUser = await user.save();

      const accessToken = await signAccessToken(savedUser.id);
      const refreshToken = await signRefreshToken(savedUser.id);

      res.send({ accessToken, refreshToken });
    } catch (error) {
      if (error.isJoi) {
        alert('Please enter valid information');
      }
      next(error);
    }
  }

  static currentAccessToken;

  static async login(req, res, next) {
    try {
      const result = await loginSchema?.validateAsync(req.body);
      const user = await User.findOne({ email: result.email });

      if (!user) {
        // throw createError.NotFound('User is not registered')
        alert('User is not registered. Please register first');
        return;
      }

      const isMatched = await user.isValidPassword(result.password);

      if (!isMatched) {
        // throw createError.Unauthorized('Username or Password is invalid');
        alert('Username or Password is invalid');
        // return;
      }

      const accessToken = await signAccessToken(user.id);
      const refreshToken = await signRefreshToken(user.id);

      PostController.currentAccessToken = accessToken;

      res.send({ accessToken, refreshToken });
    } catch (error) {
      if (error.isJoi) {
        return next(createError.BadRequest('Invalid Username or Password'));
      }
      next(error);
    }
  }

  static async getSingleUser(req, res) {
    const user = await User.findById(req.params.id);
    res.send({ user });
  }

  static async getAllUsers(req, res) {
    const users = await User.find();
    res.send({ users });
  }

  // static async updateUser(req, res) {
  //   try {
  //     const id = '' + req.params.id;
  //     const updates = req.body;
  //     const options = { new: true };

  //     const updatedUser = await User.findByIdAndUpdate(id, updates, options);
  //     res.send(updatedUser);
  //   } catch (err) {
  //     console.log(err.message);
  //   }
  // }

  // static async deleteUser(req, res) {
  //   await User.findByIdAndDelete(req.params.id);
  //   res.send('User Deleted');
  // }

  static async refreshToken(req, res, next) {
    try {
      const { refreshToken } = req.body;

      if (!refreshToken) {
        throw createError.BadRequest();
      }

      const userId = await verifyRefreshToken(refreshToken);

      const accessToken = await signAccessToken(userId);
      const refToken = await signRefreshToken(userId);

      res.send({ accessToken, refreshToken: refToken });
    } catch (error) {
      next(error);
    }
  }

  static async logout(req, res, next) {
    try {
      const { refreshToken } = req.body;

      console.log(refreshToken)
      console.log(req.body)
      if (!refreshToken) {
        throw createError.BadRequest();
      }

      const userId = await verifyRefreshToken(refreshToken);
      client.DEL(userId, (err, val) => {
        if (err) {
          console.log(err.message);
          throw createError.InternalServerError();
        }

        console.log(val);
        res.sendStatus(204);
      });
    } catch (error) {
      next(error);
    }
  }
}

module.exports = PostController;
